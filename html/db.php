<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Database</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #ADD8E6;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        nav {
            background-color: #3498db;
            padding: 15px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .logo {
            color: #fff;
            font-size: 24px;
            font-weight: bold;
            text-decoration: none;
        }

        ul {
            list-style: none;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: flex-end;
        }

        li {
            margin: 0 15px;
        }

        a {
            text-decoration: none;
            color: #fff;
            font-weight: bold;
            font-size: 16px;
            transition: color 0.3s ease-in-out;
        }

        a:hover {
            color: #ecf0f1;
        }

        .profile-container {
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            border-radius: 10px;
            text-align: center;
            margin: 20px;
        }

        .profile-picture {
            border-radius: 50%;
            width: 200px;
            height: 200px;
            object-fit: cover;
            margin-bottom: 20px;
        }

        .profile-name {
            font-size: 24px;
            font-weight: bold;
            color: #333;
            margin-bottom: 10px;
        }

        .profile-bio {
            font-size: 16px;
            color: #666;
            margin-bottom: 20px;
        }

        .profile-social {
            display: flex;
            justify-content: center;
        }

        .social-link {
            margin: 0 10px;
            text-decoration: none;
            color: #3498db;
            font-size: 20px;
        }
        .additional-pictures {
        display: flex;
        justify-content: center;
        margin-top: 20px;
        }

        .additional-pictures img {
        margin: 0 10px;
        width: 400px;
        height: 400px;
        object-fit: cover;
        }
    </style>
</head>
<body>

<nav>
    <a class="logo" href="index.html">blueiine</a>
    <ul>
        <li><a href="index.html">Home</a></li>
        <li><a href="interested.html">Interest</a></li>
        <li><a href="about_su.html">About SU</a></li>
        <li><a href="db.php">Database</a></li>
    </ul>
</nav>

<?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");
   
   echo "Connected database server<br>";
   echo "Selected database";
?>
</body>
</html>

